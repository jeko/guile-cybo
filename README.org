* Cybo - Cyber Organizer
You don't need to organize

Cybo reads a text file, formated according to the [[https://github.com/todotxt/todo.txt][todotxt]] specification,
and tells you what you have to do right here, right now.

As the file is human readable, just fill it manually (i.e. with a text
editor). Or, for the nerd around here :

#+BEGIN_SRC bash
$ echo "(A) fix critical bug of XXX" >> todo.txt
#+END_SRC

Assuming todo.txt is the file containing your todo list, here are some
step to try it.

In guile-cybo directory freshly cloned :

#+BEGIN_SRC bash
$ guile -L .
#+END_SRC

In scheme interpreter :

#+BEGIN_SRC scheme
(use-modules (cybo))
(next-action "todo.txt") ;return the next actionable task todo
(done "todo.txt") ;mark the next action todo as completed
#+END_SRC

